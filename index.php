<?php

echo "Hello world!";

$test = new Database();
$test->connect();

class Database {
    static public $dbh;

    public function connect() {
        $dbDriver = 'mysql';
        $dbHost = 'mariadb';
        $dbName = 'mydb';
        $dbUser = 'root';
        $dbPassword = 'root';

        self::$dbh = new PDO($dbDriver.':dbname='.$dbName.';host='.$dbHost, $dbUser, $dbPassword);
    }
}